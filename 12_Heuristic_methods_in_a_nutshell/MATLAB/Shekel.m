function F = Shekel(x,Data)
% Shekel.m  -- version 2018-11-05
n = Data.n;  m = Data.m; A = Data.A;  c = Data.c;  F = 0;
for j = 1:m
    S = 0;
    for i = 1:n
        S = S + (x(i) - A(i,j))^2;
    end
    F = F - 1/(c(j) + S);
end