function P1 = MatingPool(GA,P)
% MatingPool.m  -- version 2018-08-21
nP = GA.nP; nP1 = GA.nP1;
IP1 = unidrnd(nP,1,nP1); % Set of indices defining P1
P1.C = P.C(IP1,:);
P1.F = P.F(IP1);
