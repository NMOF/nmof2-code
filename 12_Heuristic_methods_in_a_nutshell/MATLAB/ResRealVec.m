% ResRealVec.m -- version 2018-11-23
fprintf(' Elapsed time %4.1f sec\n',t1);
[ign,ibest] = min(Sol); n = size(X,1);
if exist('TA','var'), P='R'; m=nR*nS; else P='G'; end
fprintf(' ------------------------\n')
for i = 1:nRe
    if i==ibest, tag = '*'; else tag = ' '; end
    if strcmp(P,'R') % -- Rounds
        j = find(~isnan(JB(:,i)),1,'last');
        p = ceil(100*( JB(j,i) / m ));
    else % -- Generations
        p = ceil(100*( JB(i) / nG ));
    end
    fprintf('%s Sol = %6.2f %3i%%  x = [',tag,Sol(i),p);
    for k = 1:n, fprintf('%5.2f ',X(k,i)); end;
    fprintf('\b]\n');
end, fprintf(' -------------------------\n'); S = Sol;
