function [Fbest,xbest,FG,rbest] = GAH(GA,Data)
% GAH.m -- version 2018-11-09
nG = GA.nG; nP2 = GA.nP2; nC = GA.nC; OF = GA.OF; SP=GA.SP;
n = Data.n; nP = GA.nP;  FG = NaN(1,nG);
[F,X,Fbest] = feval(SP,n,nP,OF,Data);
P = struct('F',F','C',X');   P2.C = false(nP2,nC);
for r = 1:nG
    P1 = MatingPool(GA,P);
    for i = 1:nP2
        P2.C(i,:) = Crossover(GA,P1);
        P2.C(i,:) = Mutate(GA,P2.C(i,:));
    end
    P = Survive(GA,P1,P2,OF,Data);
    if P.F(1) < Fbest, 
        Fbest = P.F(1); xbest = P.C(1,:); rbest = r; 
    end
    FG(r) = P.F(1);
    if (Fbest == 0) || all(~diff(P.F)), break, end %--stop
end