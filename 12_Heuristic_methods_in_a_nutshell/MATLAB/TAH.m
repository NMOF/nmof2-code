function [Fbest,xbest,FF,JB] = TAH(TA,Data,F0,x0)
% TAH.m -- version 2018-10-26
nR = TA.Rounds;  nS = TA.Steps;  OF = TA.OF;  NF = TA.NF;
FF = NaN(nR*nS,1); JB = NaN(nR,1);  JB(1) = 1; 
Fbest = F0;  xbest = x0;  FF(1) = F0;  j = 0;
for iround = 1:nR
    J = unidrnd(Data.n,nS,1);  
    for istep = 1:nS
        x1 = feval(NF,x0,J(istep),Data,TA);
        F1 = feval(OF,x1,Data);  j = j + 1;  FF(j) = F1;
        if F1 <= F0 + TA.th(iround)
            F0 = F1; x0 = x1;
            if F1 < Fbest
                Fbest = F1; xbest = x1; JB(iround) = j;
            end
        end
    end
end
