% goTA.m  -- version 2018-11-23
if ~exist('scale','var'), scale = 0.2; end
if ~exist('ptrim','var'), ptrim = 0.8; end
if ~exist('upctl','var'), upctl = 0.9; end
TA = struct('OF',OF,'SS',SS,'NF',NF,...
            'Restarts',nRe,'Rounds',nR,'Steps',nS,...
            'scale',scale,'ptrim',ptrim,'upctl',upctl);
FF  = NaN(nR*nS,nRe);  JB = NaN(nR,nRe);    n = Data.n;
Sol = NaN(nRe,1);       X = NaN(Data.n,nRe);  t0 = tic;
[Fs,xs] = feval(SS,n,nRe,OF,Data); % Starting solutions
if ~exist('th','var')
    TA.Percentiles = linspace(upctl,0,nR);
    [TA.th,ND] = thSequence(Fs(1),xs(:,1),TA,Data);
else TA.th = th; ND = []; end
for r = 1:nRe
    [Sol(r),X(:,r),FF(:,r),JB(:,r)] = TAH(TA,Data,...
                                          Fs(r),xs(:,r));
end, t1 = toc(t0);
