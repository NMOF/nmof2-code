function [th,ND] = thSequence(F0,x0,TA,Data)
% thSequence.m -- version 2018-11-23
OF = TA.OF; NF = TA.NF; nND = min(TA.Rounds*TA.Steps,5000);
ND0 = zeros(1,nND);      JJ = unidrnd(Data.n,nND,1);
for s = 1:nND
    x1 = feval(NF,x0,JJ(s),Data,TA);
    F1 = feval(OF,x1,Data);
    ND0(s) = F1 - F0;     F0 = F1;  x0 = x1;
end
ND = sort(abs(ND0)); ip = find(ND > 0,1);
ntrim = max(fix((nND-ip)*TA.ptrim),10); 
ND = ND(ip:end-ntrim);
th = quantile(ND,TA.Percentiles);  th(TA.Rounds) = 0;

