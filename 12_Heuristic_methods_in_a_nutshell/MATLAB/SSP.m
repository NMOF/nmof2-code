function F = SSP(J,Data)
% Subset sum problem  -- version 2018-08-26
F = abs(sum(Data.w(J)) - Data.s);


