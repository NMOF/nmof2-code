function [F,X,Fbest,rbest] = SlogVec(n,nR,OF,Data)
% SlogVec -- Version 2018-11-16
w = Data.w;  s = Data.s;
X = false(n,nR);  F = zeros(nR,1);  Fbest = realmax;
for r = 1:nR
    J = randperm(n); i = 1;
    while F(r) < s
        j = J(i);
        F(r) = F(r) + w(j);
        X(j,r) = true;
        i = i + 1;
    end
    if F(r) < Fbest,   Fbest = F(r); rbest = r;   end
end
