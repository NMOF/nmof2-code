function  res = converged(x0,x1,tol)
% converged.m  -- version 2007-08-10
res = all( abs(x1-x0) ./ (abs(x0) + 1) < tol );
