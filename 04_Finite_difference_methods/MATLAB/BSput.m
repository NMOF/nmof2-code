function P = BSput(S,X,r,q,T,sigma)
% BSput.m  -- version  2004-04-23
% Pricing of European put with Black-Scholes
d1 = (log(S./X) + (r-q+sigma.^2 /2) .* T) ./ (sigma.*sqrt(T));
d2 = d1 - sigma .* sqrt(T);
P  =  X .* exp(-r .* T) .* normcdf(-d2) - ...
      S .* exp(-q .* T) .* normcdf(-d1);
