function [P,Svec] = FDDownOutPut(S,X,r,q,T,sigma,Smin,Smax,M,N,theta)
%  FDDownOutPut.m  -- version  2010-11-06
%  Finite difference method for down and out put
[Am,Ap,B,Svec] = GridU1D(r,q,T,sigma,Smin,Smax,M,N,theta);
f7 = 1;
V0 = max(X - Svec,0); % Initial conditions
V0(f7+0) = 0;         % Boundary condition for down and out put
% Solve linear system for succesive time steps
[L,U] = lu(Am);
for j = M-1:-1:0
    V1 = V0;
    b = Ap*V1(f7+(1:N-1)) + theta *B*V0(f7+[0 N]) ...
                       + (1-theta)*B*V1(f7+[0 N]);
    V0(f7+(1:N-1)) = U\(L\b);
end
if nargout==2, P=V0; else P=interp1(Svec,V0,S,'spline'); end
