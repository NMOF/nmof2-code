function [P,Sf] = FDM1DAmPutsc(S,X,r,q,T,sigma,Smin,Smax,M,N,theta)
% FDM1DAmPutsc.m  -- version: 2010-10-30
% Finite difference method for American put (sparse code)
f7 = 1;  Sf = NaN(M,1);
[Am,Ap,B,Svec] = GridU1Dsc(r,q,T,sigma,Smin,Smax,M,N,theta);
V0 = max(X - Svec,0); 
Payoff = V0(f7+(1:N-1));
% Solve linear system for succesive time steps
p = Am(2:N-1,1); d = Am(:,2); q = Am(1:N-1,3);
[l,u] = lu3diag(p,d,q);
for j = M-1:-1:0
    V1 = V0;
    V0(f7+0) = X - Svec(f7+0);
    b = buildb(Ap,B,theta,V0,V1);
    % Explicit payout
    solunc = solve3diag(l,u,q,b);
    [V0(f7+(1:N-1)),Imax] = max([Payoff solunc],[],2);
    p = find(Imax == 2);
    i = p(1)-1; % Grid line of last point below payoff
    Sf(f7+j) = interp1(solunc(i:i+2)-Payoff(i:i+2),...
                           Svec(f7+(i:i+2)),0,'cubic');
end 
if ~isempty(S), 
    P = interp1(Svec,V0,S,'spline'); 
else
    P = [Svec(2:end-1) V0(f7+(1:N-1))]; 
end
