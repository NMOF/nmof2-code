function x1 = PSOR(x1,A,b,payoff,omega,tol,maxit)
% PSOR.m  -- version  2004-05-03
% Projected SOR for Ax >= b and x = max(x,payoff)
if nargin == 4, tol = 1e-4; omega = 1.7; maxit = 70; end
it = 0; N = length(x1); x0 = -x1;
while not( converged(x0,x1,tol) )
   x0 = x1;
   for i = 1:N-1
      x1(i) = omega*( b(i)-A(i,:)*x1 ) / A(i,i) + x1(i);
      x1(i) = max(payoff(i), x1(i));
   end
   it=it+1; if it>maxit, error('Maxit in PSOR'), end
end
