function [x,w] = GLanodesweights(n)
% GLanodesweights   -- version 2010-10-24
% (G_auss La_guerre...)
delta = 2*(1:n)-1; eta = 1:(n-1);
A = diag(delta) + diag(eta,1) + diag(eta,-1);
[V,D] = eig(A);
x = diag(D);
% Matlab does not guaranty sorted eigenvalues
[x,i] = sort(x);
% weights: for Laguerre, integral from 0 to infty = 1
w = V(1,i) .^ 2;