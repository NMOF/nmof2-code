function [xbest,Fbest,Fbv] = PSO(OF,Data,PS)
% PSO.m -- version 2011-01-07
if PS.NM>0
    optionsA = optimset('Display','off','MaxIter',PS.NMiter,...
                        'MaxFunEvals',PS.NMiter);
    z = @(param)calibOF(param,Data);
end

d   = PS.d; iner = PS.iner;
Fbv = NaN(PS.nG,1);  % best F-value over generations
F  = zeros(PS.nP,1); % vector of F-values members of population

v = PS.cv * randn(d,PS.nP); % initialize velocity

% construct starting population
hi_lo = Data.Dc(:,2)-Data.Dc(:,1);
P = diag(hi_lo) * rand(PS.d,PS.nP);
for i = 1:Data.d, P(i,:) = P(i,:) + Data.Dc(i,1); end

% ... and evaluate population
for i = 1:PS.nP
    F(i) = feval(OF,P(:,i),Data);
end
Pbest = P;  Fbest = F;  [Gbest,gbest] = min(F);

for k = 1:PS.nG
    v = iner*v + PS.c1 * rand(d,PS.nP) .* (Pbest - P) + ...
       PS.c2*rand(d,PS.nP) .* (Pbest(:,gbest)*ones(1,PS.nP)-P);
    v = min(v, PS.vmax); v = max(v,-PS.vmax);
    P = P + v;
    for i = 1:PS.nP
        P(:,i) = repair(P(:,i),Data);
        F(i) = feval(OF,P(:,i),Data);
        if isnan(F(i)), F(i)=1e7; end
    end
    I = F < Fbest;
    Fbest(I) = F(I); Pbest(:,I) = P(:,I);
    [Gbest,gbest] = min(Fbest); Fbv(k) = Gbest;
    % direct search
    if PS.NM>0
        if mod(k,PS.NMmod) == 0
            if PS.NMchoice==1
                [ign,nnn] = sort(F);
            elseif PS.NMchoice==2
                nnn = randperm(PS.nP);
            else
                error('choice not allowed')
            end
            for ni=1:PS.NMn
                auxF = F(nnn(ni)); diff = 1e7;
                while diff > PS.NMpres
                    paramS = P(:,nnn(ni));
                    NMsol = fminsearch(z,paramS,optionsA);
                    NMsol = repair(NMsol,Data);
                    Ftemp = calibOF(NMsol,Data);
                    if Ftemp < F(nnn(ni))
                        P(:,nnn(ni)) = NMsol;
                        if Ftemp < Fbest(nnn(ni))
                            Pbest(:,nnn(ni)) = NMsol;
                            Fbest(nnn(ni)) = Ftemp;
                        end
                        F(nnn(ni)) = Ftemp;
                        diff = abs(Ftemp-auxF);
                        auxF = Ftemp;
                    else
                        break
                    end

                end
            end
        end
        I = F < Fbest;
        Fbest(I)   = F(I);
        Pbest(:,I) = P(:,I);
        [Gbest,gbest] = min(Fbest);
        Gbest = Gbest(1); gbest = gbest(1);
        Fbv(k) = Gbest;
    end
end
xbest = Pbest(:,gbest);
Fbest = Gbest;
fprintf('Standard dev. of solutions %4.3f\n',std(Fbest))
end