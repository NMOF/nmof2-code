% changeInterval.m -- version 2010-10-24
function [x,w] = changeInterval(x, w, aFrom, bFrom, aTo, bTo)
a0 = aFrom; b0 =  bFrom;             % interval for Gauss rule
a  = aTo; b = bTo;
x = ((b-a)*x + a*b0-b*a0)/(b0-a0);   % new nodes
w = w * (b-a)/(b0-a0);               % new weights
