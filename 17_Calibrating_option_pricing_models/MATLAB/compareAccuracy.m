% compareAccuracy.m -- version 2010-11-05
%% price matrix of options
S = 100; q = 0.02; r = 0.02;
XX = 70:5:130;                     % strikes
TT = [1/12 3/12 6/12 9/12 1 2 3];  % time to maturity in years
nS = length(XX); nT = length(TT);
v  = 0.2^2;    % BS parameter (vol squared)

%% variant 1
prices1 = NaN(nS,nT);
for kk = 1:nS
    for tt = 1:nT
        prices1(kk,tt) = callBSM(S,XX(kk),TT(tt),r,q,v);
    end
end

%% variant 2
cfGeneric = @cfBSMGeneric;
param(1) = v;
from = 0; to = 200; N = 50;
[x,w] = GLnodesweights(N);
[x,w] = changeInterval(x, w, -1, 1, from, to);
prices2 = NaN(nS,nT);  % matrix of model prices
auxX    = NaN(nS,N); ix = 1i * x;
for tt = 1:nT
    tau = TT(tt);
    % evaluate CF at nodes
    CFi = S * exp((r-q) * tau);
    CF1 = cfGeneric(x - 1i,S,tau,r,q,param) ./ (ix * CFi);
    CF2 = cfGeneric(x     ,S,tau,r,q,param) ./  ix;
    for kk = 1:nS
        X = XX(kk);
        if tt == 1  % store for later maturities
            auxX(kk,:) = exp(-ix*log(X))';
        end
        P1 = 0.5 + w * real(auxX(kk,:)' .* CF1) / pi;
        P2 = 0.5 + w * real(auxX(kk,:)' .* CF2) / pi;
        prices2(kk,tt) = exp(-q * tau) * S * P1 - ...
                         exp(-r * tau) * X * P2;
    end
end

%% variant 3
prices3 = NaN(nS,nT);
for kk = 1:nS
    for tt = 1:nT
        prices3(kk,tt) = callBSMcf(S,XX(kk),TT(tt),r,q,v);
    end
end

%% compare
max(max(abs(100*(prices2-prices1))))
max(max(abs(100*(prices3-prices1))))
surf(prices2./prices1-1)