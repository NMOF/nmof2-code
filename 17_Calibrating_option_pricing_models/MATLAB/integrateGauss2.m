%% integrateGauss2.m -- version 2010-10-24
%% (also tested with Octave 4.2.2)

% goal: to compute N(b)
b = 2;

% number of nodes
n = 20;
% replace minus infinity by ...
lowerLim = -10;
% compute nodes/weights
[x,w] = GLnodesweights(n);
% change interval of integration
[x,w] = changeInterval(x, w, -1, 1, lowerLim, b);


[x2,w2] = GLanodesweights(n);

% result of integration
ourResultGLegendre = w*GaussF(x)
ourResultGLaguerre = w2 * (exp(x2).* GaussF(-(x2-b)))

% result of normcdf from Statistics Toolbox
MatlabResult = normcdf(b)

abs(ourResultGLegendre-MatlabResult)
abs(ourResultGLaguerre-MatlabResult)