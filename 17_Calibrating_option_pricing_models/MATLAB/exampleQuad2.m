% exampleQuad2.m -- version 2010-10-24
% ... continues exampleQuad1.m

% compute nodes/weights
[x,w] = GLnodesweights(m);
% change interval of integration
[x,w] = changeInterval(x, w, -1, 1, 0, 5);
fprintf('Gauss-Legendre:\t %f\n', w * Fun1(x))