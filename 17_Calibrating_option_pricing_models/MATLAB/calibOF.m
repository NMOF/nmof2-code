function [res, me] = calibOF(param,Data)
% calibOF.m -- version 2011-01-16
model = Data.model;
S = Data.S; q = Data.q; r = Data.r;
XX = Data.XX; TT = Data.TT;
prices0 = Data.prices0;
x = Data.x; w = Data.w;

if strcmp(model,'heston')
    cfGeneric = @cfHestonGeneric;
elseif strcmp(model,'bates')
    cfGeneric = @cfBatesGeneric;
elseif strcmp(model,'bsm')
    cfGeneric = @cfBSMGeneric;
elseif strcmp(model,'merton')
    cfGeneric = @cfMertonGeneric;
else
    error('model not specified')
end

% initialize structures
nS      = length(XX);  % number of strikes
nT      = length(TT);  % number of expiries
N       = length(x);   % number of nodes
Prices  = NaN(nS,nT);  % matrix of model prices
auxX    = NaN(nS,N);   %

% evaluate surface
ix = 1i * x;
for tt = 1:nT  % loop over times to maturity
    tau = TT(tt);
    % evaluate CF at nodes
    CFi = S * exp((r-q) * tau);
    CF1 = cfGeneric(x - 1i,S,tau,r,q,param) ./ (ix * CFi);
    CF2 = cfGeneric(x     ,S,tau,r,q,param) ./  ix;
    for kk = 1:nS  % loop over strikes
        if ~isnan(prices0(kk,tt))
            X = XX(kk);
            if tt == 1 % store for later maturities
                auxX(kk,:) = exp(-ix*log(X))';
            end
            P1 = 0.5 + w * real(auxX(kk,:)' .* CF1) / pi;
            P2 = 0.5 + w * real(auxX(kk,:)' .* CF2) / pi;
            Price = exp(-q * tau) * S * P1 - exp(-r*tau)*X*P2;
            Prices(kk,tt) = max(Price,0);
        end
    end
end
% replace missing values by zero
prices0(isnan(prices0)) = 0; Prices(isnan(Prices)) = 0;
% compute distance between Prices and prices0
aux = abs(Prices(:) - prices0(:));
res = mean(aux ./ prices0(:));
me  = max(aux);