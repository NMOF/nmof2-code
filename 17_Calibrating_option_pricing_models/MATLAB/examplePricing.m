% examplePricing.m -- version 2011-01-02
%% price matrix of options
S = 100; q = 0.02; r = 0.02;
XX = 70:5:130;                     % strikes
TT = [1/12 3/12 6/12 9/12 1 2 3];  % time to maturity in years
nS = length(XX); nT = length(TT);
% example parameters Bates
v0     = 0.2^2;    % current variances
vT     = 0.2^2;    % long-run variance (theta in paper)
rho    = -0.7;     % correlation
k      = 1.0;      % mean reversion speed (kappa in paper)
sigma  = 0.3;      % vol of vol
lambda = 0.1;      % intensity of jumps;
muJ    = -0.2;     % mean of jumps;
vJ     = 0.1^2;    % variance of jumps;
runs   = 100;      % for tic/toc

%% variant 1
prices1 = NaN(nS,nT);
tic
for rr = 1:runs
    for kk = 1:nS
        for tt = 1:nT
            prices1(kk,tt) = callBatescf(S,XX(kk),TT(tt), ...
                        r,q,v0,vT,rho,k,sigma,lambda,muJ,vJ);
        end
    end
end
t1 = toc;

%% variant 2
cfGeneric = @cfBatesGeneric;
param(1) = v0;
param(2) = vT;
param(3) = rho;
param(4) = k;
param(5) = sigma;
param(6) = lambda;
param(7) = muJ;
param(8) = vJ;
prices2 = NaN(nS,nT);  % matrix of model prices

from = 0; to = 200; N = 50;
[x,w] = GLnodesweights(N);
[x,w] = changeInterval(x, w, -1, 1, from, to);
auxX    = NaN(nS,N);   %

tic
for rr = 1:runs
    ix = 1i * x;
    for tt = 1:nT
        tau = TT(tt);
        % evaluate CF at nodes
        CFi = S * exp((r-q) * tau);
        CF1 = cfGeneric(x - 1i,S,tau,r,q,param) ./ (ix * CFi);
        CF2 = cfGeneric(x     ,S,tau,r,q,param) ./  ix;
        for kk = 1:nS
            X = XX(kk);
            if tt == 1  % store for later maturities
                auxX(kk,:) = exp(-ix*log(X))';
            end
            P1 = 0.5 + w * real(auxX(kk,:)' .* CF1) / pi;
            P2 = 0.5 + w * real(auxX(kk,:)' .* CF2) / pi;
            prices2(kk,tt) = exp(-q * tau) * S * P1 - ...
                             exp(-r * tau) * X * P2;
        end
    end
end
t2 = toc;  % compute speedup: t1/t2