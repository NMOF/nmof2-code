% meanVar.m -- version 2011-05-16
%% compute a mean--variance efficient portfolio (long-only)
% generate artificial returns data
ns = 60;  % number of scenarios
na = 10;  % number of assets
R = 0.005 + randn(ns, na) * 0.015;
Q = 2 * cov(R);
m = mean(R);
rd = 0.0055; % required return

c = zeros(1,na);
A = ones(1,na);         % equality constraints
a = 1;
B = [-m; -eye(na)];    % inequality constraints
b = [-rd zeros(1,na)]';

w = quadprog(Q,c,B,b,A,a);

% check constraints
sum(w), all(w>=0), m * w

%% compute and plot a whole frontier (long-only)
npoints = 100;
lambda = sqrt(1-linspace(0.99,0.05,npoints).^2);
B = -eye(na); b = zeros(1,na)';
for i = 1:npoints
    Q = 2*lambda(i) * cov(R);
    c = -(1-lambda(i)) * m;
    w = quadprog(Q,c,B,b,A,a);
    plot(sqrt(w'*cov(R)*w),m*w,'r.'), hold on
end
xlabel('Volatility')
ylabel('Expected portfolio return')
