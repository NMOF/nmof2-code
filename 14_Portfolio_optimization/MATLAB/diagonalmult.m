% diagonalmult.m -- version 2010-11-27

% set size of matrix
N = 500;

% create matrices
D = diag(rand(N, 1));
M = rand(N, N);

% compute product / compare time
tic, Z1 = D * M * D; toc
tic, Z2 = diag(D) * diag(D)' .* M; toc

% check difference between matrices
max(max(abs(Z1 - Z2)))
