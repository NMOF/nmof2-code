function F = Rho(s)
% Rho.m  -- version 2010-11-12
k = 1.58;
F = ones(length(s),1);
I = (abs(s) <= k);
temp1 = s(I)/k;
temp2 = temp1 .* temp1;
temp4 = temp2 .* temp2;
temp6 = temp4 .* temp2;
F(I) = 3*temp2 - 3*temp4 + temp6;
