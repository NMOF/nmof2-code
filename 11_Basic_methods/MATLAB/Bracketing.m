function I = Bracketing(f,xL,xU,n)
% Bracketing.m  version 2010-03-27
delta = (xU - xL)/n;
a  = xL;  k = 0; I = [];
fa = feval(f,a);
for i = 1:n
   b  = a + delta;
   fb = feval(f,b);
   if sign(fa)~=sign(fb)
      k = k + 1;
      I(k,:) = [a b];
   end
   a  = b;  fa = fb;
end

      
   
   