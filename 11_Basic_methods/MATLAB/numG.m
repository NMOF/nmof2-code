function g = numG(f,x)
% numG.m  -- version 2010-08-31
n = numel(x); g = zeros(n,1); x = x(:);
Delta = diag(max(sqrt(eps) * abs(x),sqrt(eps)));
F0 = feval(f,x);
for i = 1:n
    F1   = feval(f,x + Delta(:,i));
    g(i) = (F1 - F0) / Delta(i,i); % forward difference
end

