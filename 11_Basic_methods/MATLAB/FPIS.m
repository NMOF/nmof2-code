function S1 = FPIS(S1,r,c)
if nargin == 3, tol = 1e-4; end
it = 0; itmax = 100; S0 = -S1;
while ~converged(S0,S1,tol)
   S0 = S1 ;
   S1 = sqrt( S0^2/c * sum(Rho(r/S0)) );
   it = it + 1;
   if it > itmax, error('Maxit in FPIS'); end
end

