function c = Bisection(f,a,b,tol)
% Bisection.m  -- version 2005-05-25
% Zero finding with bisection method
if nargin == 3, tol = 1e-8; end
fa = feval(f,a); fb = feval(f,b);
if sign(fa) == sign(fb)
   error('sign f(a) is not opposite of sign f(b)');
end
done = 0;
while abs(b-a) > 2*tol & ~done
   c = a + (b - a) / 2;
   fc = feval(f,c);
   if sign(fa) ~= sign(fc)
      b  = c;
      fb = fc;
   elseif sign(fc) ~= sign(fb)
      a  = c;
      fa = fc;
   else        % center and zero coincide
      done = 1;
   end
end
