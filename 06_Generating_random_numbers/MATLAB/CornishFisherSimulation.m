function X = CornishFisherSimulation(mu,sigma,skew,kurt,N);
% CornishFisherSimulation.m  version 2011-01-06
alpha = rand(N,1);
u = norminv(alpha);
Omega = u + skew/6*(u.^2-1) + (kurt-3)/24 *(u.^3 - 3*u) ...
          - skew^2/36 * (2*u.^3-5*u);
X = mu + sigma * Omega;

