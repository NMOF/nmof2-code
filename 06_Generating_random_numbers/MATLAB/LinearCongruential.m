function u = LinearCongruential(a,b,m,seed,N);
% LinearCongruential.m  -- version 2011-01-06
%   linear congruential random number generator
%   a, c, m ... parameters
%   seed ...... seed
%   N ......... number of samples

% -- initialize
if nargin<5, N = 1;  end;
u = nan(N+1,1);
u(1) = seed;
if u(1)<1
    u(1) = floor(u(1)*m);
end

% -- generate variates
for i = 2:(N+1)
    u(i) = mod( (a*u(i-1) + c) , m);
end
u(1) = [];
u = u/m;