function x = NormalAcceptReject(n)
% NormalAcceptReject.m  -- version 2011-01-06
% 	generate n standard normal variates x
x = NaN(n,1);
for i = 1:n
    while isnan( x(i) )
        z = -log(rand);
        r = normpdf(z) / ( 1.32*exp(-z) );
        if rand < r
            x(i) = z * sign(rand - 0.5);
        end
    end
end

