% exUniforms.m -- version 2011-01-07
% generate normals, check correlations
X = randn(1000,4);
corrcoef(X)

% desired linear correlation
M =[1.0  0.7  0.6  0.6;
    0.7  1.0  0.6  0.6;
    0.6  0.6  1.0  0.8;
    0.6  0.6  0.8  1.0];

% adjust correlations for uniforms
M = 2 * sin(pi/6 .* M);

% induce correlation, check correlations
C = chol(M);
Xc = X * C;
corrcoef(Xc)

% create uniforms, check correlations
Xc(:,3:4) = normcdf(Xc(:,3:4));
corrcoef(Xc)

% plot results (marginals)
for i=1:4
    subplot(2,2,i);
    hist(Xc(:,i))
    title(['X ', int2str(i)])
end
