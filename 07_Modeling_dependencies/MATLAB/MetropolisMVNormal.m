function x = MetropolisMVNormal(n,d,s,rho)
% MetropolisMVNormal.m  -- version 2011-01-06
%   generate n standard normal variates
%   n .... number of sample
%   d .... number of dimensions
%   s .... step size for new variate
%   rho .. correlation matrix; if scalar all have same corr.)

if isscalar(rho)
    R = eye(d)*(1-rho) + ones(d)*rho; 
else
    R = rho;
end
mu = zeros(1,d);
x = nan(n,d);
x(1,:) = randn(1,d);
f_i = mvnpdf(x(1,:),mu,R);
for i = 1:(n-1)
    while isnan(x(i+1,:))
        x_new = x(i,:) + 2*(rand(1,d)-.5)*s;
        f_new = mvnpdf(x_new,mu,R);
        if rand <  f_new / f_i
            x(i+1,:) = x_new;
            f_i  = f_new;
        end
    end
end
