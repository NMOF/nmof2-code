function x = DiscreteMC(prob,N,x_0)
% DiscreteMC.m  -- version 2011-01-06
%   discrete Markov Chain with N samples
%   prob: transition variables
if nargin < 3
    uc_prob = prob^50;
    x_0 = RouletteWheel(uc_prob(1,:),1);
end
x = zeros(N+1,1);
x(1) = x_0;
for i = 1:N
    x(i+1) = RouletteWheel(prob(x(i),:),1);
end
x(1) = [];