function x = DiscreteMC2(prob_1,prob_2,N,x_0)
% DiscreteMC2.m  -- version 2011-01-06
%   discrete Markov Chain with N samples
%   prob: transition variables
if nargin < 4
    uc_prob_1 = ( prob_2 * prob_1 )^50;
    uc_prob_2 = ( prob_1 * prob_2 )^50;
    x_0(1)    = RouletteWheel(uc_prob_1(1,:),1);
    x_0(2)    = RouletteWheel(uc_prob_2(1,:),1);
end;

x = zeros(N+1,2);
x(1,:) = x_0;
for i = 1:N
    x(i+1,1) = RouletteWheel(prob_1(x(i,2),:),1);
    x(i+1,2) = RouletteWheel(prob_2(x(i,1),:),1);
end
x(1,:) = [];