% Gaussian2.m  -- version 2011-01-16
p = 5;     % number of assets
N = 500;   % number of obs
X = randn(N,p);
X = X * (diag(1./std(X)));
X = X - ones(N,1)*mean(X);
% check
plotmatrix(X); corrcoef(X)

%% induce linear correlation
rho = 0.7;   % correlation between any two assets

% set correlation matrix
M = ones(p,p) * rho;
M(1 : (p+1) : (p*p)) = 1;

% compute cholesky factor
C = chol(M);

% induce correlation, check
Xc = X * C;
plotmatrix(Xc); corrcoef(Xc)

%% rank-deficient case
rank(Xc)
Xc(:,p) = Xc(:,1:(p-1))*rand(p-1,1);
rank(Xc)
% check
plotmatrix(Xc); corrcoef(Xc)
M = corrcoef(Xc);
rank(M)
chol(M)

%% eigen decomposition
[V,D] = eig(M);
C = real(V*sqrt(D));
C = C';
Xcc = X * C;
plotmatrix(Xcc); corrcoef(Xcc)

%% eigen v. svd
% eigen decomposition
M = corrcoef(X);
[V1,D] = eig(M);
C = real(V1*sqrt(D));
C = C';

% svd
[U,S,V2] = svd(X);

% ratio of sing values squared to eigenvalues
((diag(S).^2)/(N-1)) ./ sort(diag(D),'descend')