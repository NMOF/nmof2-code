% lognormals.m -- version 2010-01-08
%% uncorrelated Gaussian variates
p = 3; N = 200; X = randn(N,p);
X = X * (diag(1./std(X)));
X = X - ones(N,1)*mean(X);
figure(1)
plotmatrix(X); corrcoef(X)

%% induce linear correlation
rho = 0.5; M = ones(p,p) * rho;
M(1:(p + 1):(p * p)) = 1;
C = chol(M); Xc = X * C;
figure(2)
plotmatrix(Xc); corrcoef(Xc)

%% make exp
Z = exp(Xc);
figure(3)
plotmatrix(Z); corrcoef(Z)

%% change variance of Xc
sd = 5;
Xc(:,1) = sd*Xc(:,1);
Z = exp(Xc);
figure(4)
plotmatrix(Z); corrcoef(Z)