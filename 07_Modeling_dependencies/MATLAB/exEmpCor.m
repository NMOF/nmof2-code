% exEmpCor.m -- version 2011-01-04
NO = 200; % empirical number of obs
Y1 = randn(NO,1);
Y2 = randn(NO,1); Y2(Y2<0.3) = -0.5; % make Y2 non-Gaussian

% create CDFs
sortedY1 = sort(Y1);
sortedY2 = sort(Y2);

% resample
N = 1000; % (re)sample size
Z = randn(N,2); rho = 0.9;
M = [1 rho; rho 1]; Z = Z*chol(M);
U = normcdf(Z); U = ceil(NO*U);

%check
corrcoef(Y1,Y2)
corrcoef(sortedY1(U(:,1)),sortedY2(U(:,2)))

% histograms and scatter of original Y1 and Y2
subplot(231), hist(Y1)
subplot(232), hist(Y2)
subplot(233),scatter(Y1(U(:,1)),Y2(U(:,2)))

% histograms and scatter of resampled/correlated Y1 and Y2
subplot(234), hist(Y1(U(:,1)))
subplot(235), hist(Y2(U(:,2)))
subplot(236),scatter(sortedY1(U(:,1)),sortedY2(U(:,2)))