function SimStk = SimulateStk(mu,sigma,N_samples)
% SimulateStk.m  -- version 2011-01-10
% mu, sigma ....: drift and volatility
% N ............: number of samples
SimStk.mu = mu;
SimStk.sigma = sigma;
SimStk.r = randn(N_samples,1)*sigma + mu;
SimStk.S = exp(SimStk.r);
SimStk.meanr = mean(SimStk.r);
SimStk.stdr  = std(SimStk.r);
SimStk.meanS = mean(SimStk.S);
SimStk.muS   = exp(mu+sigma^2/2);
SimStk.medS  = median(SimStk.S);

r = SimStk.r; S_T = SimStk.S;
% display results
fprintf('        mean: %4.2f (mu   = %4.2f)\n',SimStk.meanr,mu);
fprintf('  volatility: %4.2f (E(r) = %4.2f)\n',SimStk.stdr,sigma);
fprintf('  exp. price: %4.2f (E(S) = %4.2f)\n',SimStk.meanS,exp(mu+sigma^2/2));
fprintf('median price: %4.2f (M(S) = %4.2f)\n',SimStk.medS,exp(mu));
