function [r,e,h] = GARCHsim(mu,a0,a1,b1,T,Tb)
% GARCHsim.m  -- version 2011-01-06
if nargin < 6, Tb = 0; end  % no "before" periods to swing in
% -- initialize variables
z = randn(T+Tb,1);
e = zeros(T+Tb,1);
h = zeros(T+Tb,1);
h(1) = a0/(1-(a1+b1));
e(1) = z(1) * sqrt(h(1));
% -- generate sample variances and innovations
for t = 2:(T+Tb)
    h(t) = a0 + a1 * e(t-1)^2 + b1 * h(t-1);
    e(t) = z(t) * sqrt(h(t));
end
% -- remove excess observations from initialization phase
e(1:Tb,:) = [];
h(1:Tb,:) = [];
% -- compute returns
r = e + mu;
