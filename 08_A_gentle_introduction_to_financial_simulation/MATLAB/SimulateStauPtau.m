function [PF_tau,S_tau,p_tau] = SimulateStauPtau(S_0,drift,vol,X,rSafe,T,tau,NSample)
% SimulateStauPtau.m  -- version 2011-01-06
%   simulate portfolio with  1 stock and 1 put at time tau
%   S_0       initial stock price with return ~ N(drift,vol^2)
%   X         strike of put
%   rSafe     riskfree rate of return
%   T         time to maturity of put
%   tau       point of valuation; if not provided: tau = T
%   NSample   number of samples for the simulation
if nargin < 7, tau = T;  else tau = min(T,tau); end
if nargin < 8, NSample = 10000; end;
% -- simulate stock prices at tau
r = randn(NSample,1)*(vol*sqrt(tau)) + drift * tau;
S_tau = S_0 * exp(r);
% -- compute put prices at tau; time left to maturity (T-tau)
T_left = T - tau;
p_tau = BSput(S_tau,X,rSafe,0,T_left,vol);
% -- value of portfolio at time tau
PF_tau  = S_tau + p_tau;
