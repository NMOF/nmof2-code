% EuropeanOptionSimulation.m  -- version 2011-01-06

% parameters
S0  = 100;
X   = 100;
rf  = .05;
sigma = .2;
T   = .25;
ns  = 100000; % number of Monte Carlo samples

% Black-Scholes price for European call
d1   = (log(S0/X)+(rf+sigma^2/2)*T) / (sigma * sqrt(T));
d2   = d1 - sigma*sqrt(T);
c0BS = S0*normcdf(d1) - X*exp(-rf*T) * normcdf(d2);

% MC Simulation
rs   = randn(ns,1)*sigma*sqrt(T) + (rf - sigma^2/2)*T;
STs  = S0*exp(rs);
cTs  = max(STs-X,0);
c0MC = mean(cTs)*exp(-rf*T);

% "ingredients"
ex   = STs>X;    % Boolean: exercise yes/no

fprintf('Simulation Results: \n======================\n');
fprintf('E(stock price): %8.4f (theor.: %8.3f)\n', ...
    [mean(STs) S0*exp(rf*T)]); 
fprintf('prob. exercise: %8.4f (theor.: %8.3f)\n', ...
    [mean(ex) normcdf(d2)]);
fprintf('PV(E(paymt X)): %8.4f (theor.: %8.3f)\n', ...
    [mean(ex)*X*exp(-rf*T) X*exp(-rf*T) * normcdf(d2)]);
fprintf('PV(E(paymt S)): %8.4f (theor.: %8.3f)\n', ...
    [mean(ex)*mean(STs(ex))*exp(-rf*T) S0*normcdf(d1)]);
fprintf('======================\ncall price:     %8.4f (theor.: %8.3f)\n', ...
    [c0MC c0BS]);


