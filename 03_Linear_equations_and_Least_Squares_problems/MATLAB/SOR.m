function [x1,nit] = SOR(x1,A,b,omega,tol,maxit)
% SOR.m  -- version 2010-10-25
% SOR for Ax = b 
if nargin == 3, tol=1e-4; omega=1.2; maxit=70; end
it = 0; n = length(x1); x0 = -x1;
while ~converged(x0,x1,tol)
   x0 = x1;
   for i = 1:n
      x1(i) = omega*( b(i)-A(i,:)*x1 ) / A(i,i) + x1(i);
   end
   it=it+1; if it>maxit, error('Maxit in SOR'), end
end
if nargout == 2, nit = it; end
