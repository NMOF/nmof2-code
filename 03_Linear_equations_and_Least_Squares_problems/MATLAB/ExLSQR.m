% ExLSQR.m  -- version 1995-11-25
A=[1 1; 1 2; 1 3; 1 4]; b=[2 1 1 1]';
[m,n] = size(A);
[Q,R] = qr(A);
R1 = R(1:n,1:n); Q1 = Q(:,1:n); Q2 = Q(:,n+1:m);
x = R1\(Q1'*b)
r = Q2'*b;
sigma2 = r'*r/(m-n)
T = trilinv(R1');
S = T'*T;
Mcov = sigma2*S
