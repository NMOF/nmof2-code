function paths = pricepaths(S,tau,r,q,v,M,N)
% pricepaths.m -- version 2010-12-08
%   S   = spot
%   tau = time to mat
%   r   = riskfree rate
%   q   = dividend yield
%   v   = volatility^2
%   M   = time steps
%   N   = number of paths
dt = tau/M;
g1 = (r - q - v/2)*dt; g2 = sqrt(v * dt);
aux = cumsum([log(S)*ones(1,N); g1 + g2 * randn(M,N)],1);
paths = exp(aux);
