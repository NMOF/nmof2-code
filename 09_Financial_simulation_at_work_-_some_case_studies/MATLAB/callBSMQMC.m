function call = callBSMQMC(S,K,tau,r,q,v,N)
% callBSMQMC.m -- version 2010-12-08
% S   = spot
% X   = strike
% tau = time to mat
% r   = riskfree rate
% q   = dividend yield
% v   = volatility^2
g1 = (r - q - v/2)*tau; g2 = sqrt(v*tau);
U  = VDC(1:N,7);
ee = g2 * norminv(U);
z  = log(S) + g1 + ee;
S  = exp(z);
payoff = exp(-r*tau) * max(S-K,0);
call = sum(payoff)/N;