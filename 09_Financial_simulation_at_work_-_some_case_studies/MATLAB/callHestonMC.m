function [call,Q] = callHestonMC(S,X,tau,r,q,v0,vT,rho,k,sigma,M,N)
% callHestonMC.m -- version 2011-01-08
% S     = spot
% X     = strike
% tau   = time to maturity
% r     = riskfree rate
% q     = dividend yield
% v0    = initial variance
% vT    = long run variance (theta in Heston's paper)
% rho   = correlation
% k     = speed of mean reversion (kappa in Heston's paper)
% sigma = vol of vol
% M     = time steps
% N     = number of paths
dt = tau/M; sumPayoff = 0;
C = [1 rho;rho 1]; C = chol(C);
T = 0; Q = 0;
for n = 1:N
    ee = randn(M,2);
    ee = ee * C;
    vS = log(S); vV = v0;
    for t = 1:M
        % --update stock price
        dS = (r - q - vV/2)*dt + sqrt(vV)*ee(t,1)*sqrt(dt);
        vS = vS + dS;
        % --update squared vol
        aux = ee(t,2);
        % --Euler scheme
        dV = k*(vT-vV)*dt + sigma*sqrt(vV)*aux*sqrt(dt);
        % --absorbing condition
        if vV + dV < 0
            vV = 0;
        else
            vV = vV + dV;
        end
        % --zero variance: some alternatives
        %if vV + dV < 0, dV = k*(vT-vV)*dt;end;vV = vV + dV;
        %if vV + dV <= 0, dV = k*(vT)*dt;end;vV = vV + dV;
    end
    Send = exp(vS);
    payoff = max(Send-X,0);
    sumPayoff = payoff + sumPayoff;
    %compute variance
    if n > 1
        T = T + payoff;
        Q = Q + (1/(n*(n-1))) * (n*payoff - T)^2;
    else
        T = payoff;
    end
end
call = exp(-r*tau) * (sumPayoff/N);