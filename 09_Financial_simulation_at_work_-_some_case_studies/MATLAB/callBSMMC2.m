function [call,Q]= callBSMMC2(S,X,tau,r,q,v,M,N)
% callBSMMC2.m -- version 2010-12-10
% S   = spot
% X   = strike
% tau = time to maturity
% r   = riskfree rate
% q   = dividend yield
% v   = volatility^2
% M   = time steps
% N   = number of paths
dt = tau/M;
g1 = (r - q - v/2)*dt; g2 = sqrt(v*dt);
sumPayoff = 0; T = 0; Q = 0;
s = log(S);
for n = 1:N
    z = g1 + g2*randn(M,1);
    z = cumsum(z)+s;
    Send = exp(z(end));
    payoff = max(Send-X,0);
    sumPayoff = payoff + sumPayoff;
    %compute variance
    if n > 1
        T = T + payoff;
        Q = Q + (1/(n*(n-1))) * (n*payoff - T)^2;
    else
        T = payoff;
    end
end
call = exp(-r*tau) * (sumPayoff/N);