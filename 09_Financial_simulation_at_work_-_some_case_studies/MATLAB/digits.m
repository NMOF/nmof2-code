function dd = digits(k,b)
% digits.m -- version 2010-12-08
nD = 1 + floor(log(max(k))/log(b)); % required digits
dd = zeros(length(k),nD);
for i = nD:-1:1
    dd(:,i) = mod(k,b);
    if i>1; k = fix(k/b); end
end
