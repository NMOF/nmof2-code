function P0 = AmericanPut(S0,X,r,T,sigma,M)
% AmericanPut.m -- version 2010-12-28
f7 = 1;  dt = T / M;  v = exp(-r * dt);
u = exp(sigma * sqrt(dt));   d = 1 / u;
p = (exp(r * dt) - d) / (u - d);

% initialize asset prices at maturity (period M)
S = zeros(M + 1,1);
S(f7+0) = S0 * d^M;
for j = 1:M
    S(f7+j) = S(f7+j - 1) * u / d;
end

% initialize option values at maturity (period M)
P = max(X - S, 0);

% step back through the tree
for i = M-1:-1:0
    for j = 0:i
        P(f7+j) = v * (p * P(f7+j + 1) + (1-p) * P(f7+j));
        S(f7+j) = S(f7+j) / d;
        P(f7+j) = max(P(f7 + j),X - S(f7+j));
    end
end
P0 = P(f7+0);